import {Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/retry';
import {AppService} from "../shared/app.services";
@Component({
    selector: 'app-product-delivery',
    templateUrl: './product-delivery.component.html',
    styleUrls: ['./product-delivery.component.css']
})
export class ProductDeliveryComponent implements OnInit {
    public _timeArray: any;
    public _baseTime = 'https://vk-shop-b8bba.firebaseio.com/delivery.json';
    constructor(private _appService: AppService) {
    }

    ngOnInit() {
        this._appService.getData(this._baseTime).retry(3).subscribe(data=>{
            this._timeArray = data;
        });
    }
}
