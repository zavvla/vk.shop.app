import {RouterModule, Routes} from '@angular/router';
import {ProductListComponent} from '../product-list/product-list.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {ProductBasketComponent} from '../product-basket/product-basket.component';
import {ProductActionComponent} from '../product-action/product-action.component';
import {ProductDeliveryComponent} from '../product-delivery/product-delivery.component';
import {CallbackFormComponent} from '../callback-form/callback-form.component';
import {ContactInfoComponent} from "../contact-info/contact-info.component";
import {AppErrorComponent} from "../app-error/app-error.component";
import {AdministratorComponent} from "../administrator/administrator.component";

const APP_ROUTES: Routes = [
    {path: '', redirectTo: 'goods', pathMatch: 'full'},
    {path: 'goods', component: ProductListComponent},
    {path: 'basket', component: ProductBasketComponent},
    {path: 'action', component: ProductActionComponent},
    {path: 'delivery', component: ProductDeliveryComponent},
    {path: 'order', component: CallbackFormComponent},
    {path: 'contact', component: ContactInfoComponent},
    {path: 'error', component: AppErrorComponent},
    {path: 'admin', component: AdministratorComponent},
    {path: '**', component: NotFoundComponent}
];

export const APP_ROUTER = RouterModule.forRoot(APP_ROUTES, {enableTracing: false});
