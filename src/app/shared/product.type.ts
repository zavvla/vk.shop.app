export class Product {
    constructor(public productId: string,
                public productImage: string,
                public productTitle: string,
                public productAction: boolean,
                public productWeight: string,
                public productPrice: string,
                public productAvailable: boolean,
                public productConsist: Array<any>) {
    }
}