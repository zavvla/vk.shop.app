import {Injectable, Output, EventEmitter} from '@angular/core';
import {Http, Response, Headers} from "@angular/http";
import "rxjs/Rx";
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs/BehaviorSubject";


@Injectable()
export class AppService {
    constructor(private _http: Http, private _router: Router) {
    }

    /*LET------------------------------------------------------*/

        public baseOrder = 'https://vk-shop-b8bba.firebaseio.com/order.json';


    /*EMMIT----------------------------------------------------*/
        @Output() _emitUpdateCountProduct = new EventEmitter();
        @Output() _emitCartAnimate = new EventEmitter();
        @Output() _emitFavoriteAnimate = new EventEmitter();
        @Output() _emitFavoriteCount = new EventEmitter();
        @Output() _emitUpdateSearchCount = new EventEmitter();

        onEmitChangeData(emitCountProduct: any, emitCartAnimate?: boolean) {
            this._emitUpdateCountProduct.emit(emitCountProduct);
            this._emitCartAnimate.emit(emitCartAnimate);
        }

        onEmitChangeFavorite(emitCountFavorite: any, emitFavoriteAnimate?: boolean){
            this._emitFavoriteCount.emit(emitCountFavorite);
            this._emitFavoriteAnimate.emit(emitFavoriteAnimate);
        }

        onUpdateSearchCount(emitSearchFound: any){
            this._emitUpdateSearchCount.emit(emitSearchFound);
        }

    /*GET------------------------------------------------------*/
        getData(url: string) {
            return this._http.get(url).map((response: Response) => {
                return response.json();
            })
        }
    /*POST------------------------------------------------------*/
        sendOrder(order: any) {
            const headers = new Headers({
                'Content-Type': 'application/json'
            });
            return this._http.post(this.baseOrder, JSON.stringify(order), {headers: headers})
        }
    /*SERVICE------------------------------------------------------*/
        private __loggerSource = new BehaviorSubject<number>(0);
        __loggerError$ = this.__loggerSource.asObservable();

        onLoggerError(code: number) {
            this.__loggerSource.next(code);
            this._router.navigate(['error']);
        }

}
