export class ProductListType {
    constructor(public productId: string,
                public productImage: string,
                public productTitle: string,
                public productWeight: string,
                public productPrice: string,
                public productAvailable: boolean,
                public productFavorites: boolean,
                public productConsist: Array<any>) {
    }
}