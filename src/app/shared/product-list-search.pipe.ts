import {Pipe, PipeTransform} from '@angular/core';
import {AppService} from "./app.services";

@Pipe({
    name: 'productListSearch'
})
export class ProductListSearchPipe implements PipeTransform {
    constructor(private _appService: AppService){

    }
    transform(inputObject: any, inputArgs?: any): any {
        if (inputArgs) {
            inputArgs = inputArgs.toLowerCase();
            let returnObject = inputObject.filter(function (el: any) {
                return el.productTitle.toLowerCase().indexOf(inputArgs) > -1;
            });
            this._appService.onUpdateSearchCount(returnObject.length);
            return returnObject;
        }else{
            this._appService.onUpdateSearchCount(0);
        }
        return inputObject;
    }
}
