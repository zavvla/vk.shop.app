import {Component, OnInit} from '@angular/core';
import {AppService} from "../shared/export.barrel";
import {Subscription} from "rxjs/Subscription";

@Component({
    selector: 'app-app-error',
    templateUrl: './app-error.component.html',
    styleUrls: ['./app-error.component.css']
})
export class AppErrorComponent implements OnInit {
    public _messageErrorUser;
    public _subscription: Subscription;

    constructor(private _appService: AppService) {
    }

    ngOnInit() {
        this._subscription = this._appService.__loggerError$
            .subscribe(data => {
                switch (data) {
                    case 1: {
                        this._messageErrorUser = 'Не смогли загрузить список вкусняшек. У Вас точно работает интернет?';
                        break;
                    }
                    case 2: {
                        this._messageErrorUser = 'Не удалось сохранить данные локально.';
                        break;
                    }
                    case 3: {
                        this._messageErrorUser = 'Но вы же ничего не заказали, Ваша корзина пуста!';
                        break;
                    }
                    case 4: {
                        this._messageErrorUser = 'Не удалось отправить заказ.';
                        break;
                    }
                    case 5: {
                        this._messageErrorUser = 'Не удалось добавить в избранное.';
                        break;
                    }
                    default: {
                        this._messageErrorUser = 'Наши парни уже исправляют ошибку!';
                        break;
                    }
                }
            },err =>{

            });
    }
}
