import {Component, OnInit} from '@angular/core';
import {AppService} from "./shared/app.services";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public _productCount: number = 0;
    public _successAdd: boolean = false;
    public _favoriteAdd: boolean = false;
    public _favoriteCount: number = 0;
    public _openMenu: boolean = false;

    constructor(private _appService: AppService) {
    }

    ngOnInit() {
        this._appService._emitUpdateCountProduct.subscribe(data => {
            this._productCount = data;
        });
        this._appService._emitFavoriteCount.subscribe(data => {
            this._favoriteCount = data;
        });
        this._appService._emitCartAnimate.subscribe(data => {
            this._successAdd = data;
            setTimeout(() => {
                this._successAdd = false;
            }, 1000);
        });
        this._appService._emitFavoriteAnimate.subscribe(data => {
            this._favoriteAdd = data;
            setTimeout(() => {
                this._favoriteAdd = false;
            }, 1000);
        });
    }

    onOpenMenu() {
        this._openMenu = !this._openMenu;
    }
}
