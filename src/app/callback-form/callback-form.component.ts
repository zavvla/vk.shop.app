import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AppService} from "../shared/app.services";
import {Router} from "@angular/router";
import * as _ from "lodash";

@Component({
    selector: 'app-callback-form',
    templateUrl: './callback-form.component.html',
    styleUrls: ['./callback-form.component.css']
})
export class CallbackFormComponent implements OnInit {
    public _eventForm: FormGroup;
    public _productArray: Array<any> = [];
    public _userContact: any = [];
    public _orderArray: any = {};
    public _orderNumber: any;
    public _statusOrder: boolean = false;

    private _formConstructorAsync() {
        let userName = null,
            userPhone = null,
            userAddress = null,
            userComment = null;

        if (this._userContact) {
            userName = this._userContact.userName;
            userPhone = this._userContact.userPhone;
            userAddress = this._userContact.userAddress;
        }

        this._eventForm = this._formBuilder.group({
            userName: [userName, [Validators.required, Validators.maxLength(120)]],
            userPhone: [userPhone, [Validators.required, Validators.maxLength(50)]],
            userAddress: [userAddress, [Validators.required, Validators.maxLength(200)]],
            userComment: [userComment, Validators.maxLength(500)],
        });
    }

    private dynamicProductCount(emmitCartAnimate?: boolean) {
        let countProducts = 0;
        _.forEach(this._productArray, function (value) {
            countProducts = countProducts + value.productCount
        });
        this._appService.onEmitChangeData(countProducts, emmitCartAnimate)
    }

    constructor(private _appService: AppService,
                private _router: Router,
                private _formBuilder: FormBuilder) {
    }

    ngOnInit() {
        if (!_.isNull(JSON.parse(localStorage.getItem("cartProducts")))) {
            this._productArray = JSON.parse(localStorage.getItem("cartProducts"));
            this.dynamicProductCount(false);
        }else{
            this._router.navigate(['/']);
        }
        if (!_.isNull(JSON.parse(localStorage.getItem("_userContact")))) {
            this._userContact = JSON.parse(localStorage.getItem("_userContact"));
        }
        this._formConstructorAsync();
    }

    onSubmit() {
        if(this._productArray.length > 0){
            this._orderArray['data'] = this._productArray;
            this._orderArray['contact'] = this._eventForm.value;
            this._appService.sendOrder(this._orderArray).subscribe(
                data =>{
                    if(data.status == 200){
                        this._orderNumber = data.json().name;
                        localStorage.removeItem("cartProducts");
                        this._statusOrder = true;
                        delete this._orderArray;
                        delete this._productArray;
                        this.dynamicProductCount(false);
                        try {
                            localStorage.setItem('_userContact', JSON.stringify(this._eventForm.value));
                        } catch (e) {
                            this._appService.onLoggerError(2);
                        }
                    }
                    else{
                        this._statusOrder = false;
                        this._appService.onLoggerError(4);
                    }
                }
            )
        }else{
            this._appService.onLoggerError(3);
        }
    }
}
