import {Component, OnInit} from '@angular/core';
import {AppService} from "../shared/app.services";

@Component({
    selector: 'app-contact-info',
    templateUrl: './contact-info.component.html',
    styleUrls: ['./contact-info.component.css']
})
export class ContactInfoComponent implements OnInit {
    public _contactArray: any;
    public _baseContact = 'https://vk-shop-b8bba.firebaseio.com/contact.json';

    constructor(private _appService: AppService) {
    }

    ngOnInit() {
        this._appService.getData(this._baseContact).subscribe(
            data => {
                this._contactArray = data;
            }, error => {

            }, function () {

            })
    }

}
