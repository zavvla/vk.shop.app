import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {APP_ROUTER} from './shared/app.routes';
import {AppService} from './shared/app.services';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {ProductListComponent} from './product-list/product-list.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ProductActionComponent} from './product-action/product-action.component';
import {ProductDeliveryComponent} from './product-delivery/product-delivery.component';
import {ProductBasketComponent} from './product-basket/product-basket.component';
import {CallbackFormComponent} from './callback-form/callback-form.component';
import {ContactInfoComponent} from './contact-info/contact-info.component';
import {AppErrorComponent} from './app-error/app-error.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { ProductListSearchPipe } from './shared/product-list-search.pipe';


@NgModule({
    declarations: [
        AppComponent,
        ProductListComponent,
        NotFoundComponent,
        ProductActionComponent,
        ProductDeliveryComponent,
        ProductBasketComponent,
        CallbackFormComponent,
        ContactInfoComponent,
        AppErrorComponent,
        AdministratorComponent,
        ProductListSearchPipe
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        APP_ROUTER
    ],
    providers: [AppService],
    bootstrap: [AppComponent]
})

export class AppModule {

}
