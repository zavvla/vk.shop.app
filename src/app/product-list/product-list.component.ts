import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppService, ProductListType} from '../shared/export.barrel';
import * as _ from "lodash";
import 'rxjs/add/operator/retry';

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {
    public _productList: ProductListType[];
    public _toggleConsist: Array<boolean> = [];
    public _productArray: Array<any> = [];
    public _favoritesArray: Array<any> = [];
    public _filterSearch: string;
    public _filterSearchIndex: any = 0;
    public _baseUrl = 'https://vk-shop-b8bba.firebaseio.com/data.json';

    private dynamicProductCount(emmitCartAnimate?: boolean) {
        let countProducts = 0;
        _.forEach(this._productArray, function (value) {
            countProducts = countProducts + value.productCount
        });
        this._appService.onEmitChangeData(countProducts, emmitCartAnimate)
    }

    private dynamicFavoriteCount(emitFavoriteAnimate?: boolean){
        this._appService.onEmitChangeFavorite(this._favoritesArray.length, emitFavoriteAnimate)
    }

    constructor(private _appService: AppService,
                private _router: Router) {
    }

    ngOnInit() {
        this._appService.getData(this._baseUrl).retry(3).subscribe(
            data => {
                _.forEach(this._favoritesArray, function(product) {
                    let _loadFavorites = _.find(data, ['productId', product.productId]);
                    _loadFavorites["productFavorites"] = true;
                });

                this._productList = data;

                if (!_.isNull(JSON.parse(localStorage.getItem("cartProducts")))) {
                    this._productArray = JSON.parse(localStorage.getItem("cartProducts"));
                    this.dynamicProductCount(false);
                }
            },
            err => {
                this._appService.onLoggerError(1);
            });

        if (!_.isNull(JSON.parse(localStorage.getItem("cartFavorites")))) {
            this._favoritesArray = JSON.parse(localStorage.getItem("cartFavorites"));
            this.dynamicFavoriteCount(false)
        }

        this._appService._emitUpdateSearchCount.subscribe(data =>{
            this._filterSearchIndex = data;
        });
    }

    addFavorites(product: any){
        if (_.some(this._favoritesArray, ['productId', product.productId])) {
            this._favoritesArray.splice(this._favoritesArray.indexOf(_.find(this._favoritesArray, ['productId', product.productId])), 1);
            product.productFavorites = false;
        }else{
            this._favoritesArray.push({
                productId: product.productId
            });
            product.productFavorites = true;
        }
        try {
            localStorage.setItem('cartFavorites', JSON.stringify(this._favoritesArray));
            this.dynamicFavoriteCount(true)
        } catch (e) {
            this._appService.onLoggerError(5);
        }
    }

    onBasket(product: any) {
        if (product.productAvailable !== false) {
            let countProducts = 1;
            if (this._productArray.length) {
                if (_.some(this._productArray, ['productId', product.productId])) {
                    let currentCountProduct = _.find(this._productArray, ['productId', product.productId]);
                    currentCountProduct.productCount++;
                } else {
                    this._productArray.push({
                        productId: product.productId,
                        productImage: product.productImage,
                        productTitle: product.productTitle,
                        productPrice: product.productPrice,
                        productWeight: product.productWeight,
                        productFavorites: product.productFavorites,
                        productCount: countProducts
                    });
                }
            } else {
                this._productArray.push({
                    productId: product.productId,
                    productImage: product.productImage,
                    productTitle: product.productTitle,
                    productPrice: product.productPrice,
                    productWeight: product.productWeight,
                    productFavorites: product.productFavorites,
                    productCount: countProducts
                });
            }
            try {
                localStorage.setItem('cartProducts', JSON.stringify(this._productArray));
                this.dynamicProductCount(true);
            } catch (e) {
                //todo @zavvla сделать компонент обработки ошибок
                //if (e == QUOTA_EXCEEDED_ERR) {console.log('Превышен лимит');}
                this._appService.onLoggerError(2);
            }
        }
    }

    onToBasket(){
        this._router.navigate(['basket'])
    }
}
