import {Component, OnInit} from '@angular/core';
import {AppService} from "../shared/app.services";
import {Router} from "@angular/router";
import * as _ from "lodash";


@Component({
    selector: 'app-product-basket',
    templateUrl: './product-basket.component.html',
    styleUrls: ['./product-basket.component.css']
})
export class ProductBasketComponent implements OnInit {
    public _productArray: Array<any> = [];
    public _allTotalList: any;
    public _allTotal: any;
    public _discount: any = "—";
    public _delivery: any = 600;

    constructor(private _appService: AppService,
                private _router: Router) {
    }

    ngOnInit() {
        if (!_.isNull(JSON.parse(localStorage.getItem("cartProducts")))) {
            this._productArray = JSON.parse(localStorage.getItem("cartProducts"));
            this.dynamicProductCount(false);
        } else {
            this.dynamicProductCount(false);
            this._router.navigate(['/']);
        }
    }

    onIncrement(product: any) {
        let currentCountProduct = _.find(this._productArray, ['productId', product.productId]);
        currentCountProduct.productCount++;
        this.dynamicProductCount(true);
    }

    onDecrement(product: any) {
        let currentCountProduct = _.find(this._productArray, ['productId', product.productId]);
        if (currentCountProduct.productCount > 1) {
            currentCountProduct.productCount--;
            this.dynamicProductCount(true);
        } else {
            this._productArray.splice(this._productArray.indexOf(product), 1);
            this.dynamicProductCount(true);
            if (this._productArray.length < 1) {
                this._router.navigate(['/']);
            }
        }
    }

    onDelete(product: any) {
        this._productArray.splice(this._productArray.indexOf(product), 1);
        this.dynamicProductCount(true);
        if (this._productArray.length < 1) {
            this._router.navigate(['/']);
        }
    }

    private dynamicProductCount(emitCartAnimate?: boolean) {
        let countProducts = 0,
            allTotalList = 0;

        _.forEach(this._productArray, function (value) {
            countProducts = countProducts + value.productCount;
            allTotalList = allTotalList + (countProducts * value.productPrice);
        });
        this._appService.onEmitChangeData(countProducts, emitCartAnimate);

        this._allTotalList = allTotalList;
        this._allTotal = allTotalList + this._delivery;

        try {
            localStorage.setItem('cartProducts', JSON.stringify(this._productArray));
        } catch (e) {
            this._appService.onLoggerError(2);
        }
    }
}



