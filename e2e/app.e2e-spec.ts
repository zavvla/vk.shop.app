import { Zavvla.Github.IoPage } from './app.po';

describe('zavvla.github.io App', () => {
  let page: Zavvla.Github.IoPage;

  beforeEach(() => {
    page = new Zavvla.Github.IoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
